#!/opt/homebrew/opt/ruby/bin/ruby

elf_to_calories = {}
elf_num = 0

open("#{__dir__}/input-1.txt").each_line do |line|

  line.chomp!  # take off the newlines

  # on empty line, assume new elf
  if line.empty?
    elf_num += 1
  else
    elf_to_calories[elf_num] = 0 unless elf_to_calories.has_key? elf_num
    elf_to_calories[elf_num] += line.to_i
  end
end

# part 1 solution
p elf_to_calories.values.max

# part 2 solution
p elf_to_calories.values.sort[-3..-1].reduce(0) { |sum, calories| sum += calories }